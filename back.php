<?php
error_reporting(E_ALL);
class Former{
    private $name;
    private $email;
    private $message;
    private $file;
    public function __construct($name,$email,$message,$file)
    {
        $this->name = $name;
        $this->email = $email;
        $this->message = $message;
        $this->file = $file;
    }
    private function returntype(string $file){ //тут путь
        if(mime_content_type($file) === 'image/png'){
            return '.png';
        }
        elseif(mime_content_type($file) === 'image/jpeg'){
            return '.jpg';
        }
    }
    public function processing()
    {
        $error = [];   
        if(empty($this->name) || empty($this->email) || empty($this->message)){

            $error[] = 'something empty';
        }
        if(preg_match('#^[a-zA-Z0-9]+$#',$this->name) === 0){

            $error[] = 'invalid name';
        }
        if(preg_match('#^([a-zA-Z0-9.]+\@([a-z]{2,10})\.([a-z]{2,5}))$#',$this->email) === 0){

            $error[] = 'invalid email';
        }
        if(preg_match('#^[ЁёА-Яа-я0-9A-Za-z]+$#u',$this->message) === 0){

            $error[] = 'invalid message';
        }
        if(!empty($this->file['tmp_name'])){

            if(mime_content_type($this->file['tmp_name']) === 'image/jpeg' ||
                mime_content_type($this->file['tmp_name']) === 'image/png'){
                    $bytes = random_bytes(5);
                    $name = bin2hex($bytes);
                    $format = $this->returnType($this->file['tmp_name']);
                    $filenamee = $name .$format;
                    move_uploaded_file($this->file['tmp_name'],'images/'.$filenamee);
                    $this->message = $this->message . "\n". 'images/' .$filenamee;
            }    
        }
        if(empty($error)){
            $name = 'messages/'.$this->name . date("YmdHis") . '.txt';
            $text_file = fopen($name,'w');
            fwrite($text_file,$this->message);
            fclose($text_file);
            $arr = ['успех'];
            return $arr;



        } else{
            return $error;
        }
    }
}

    if(!isset($_FILES['file_r'])){
        $_FILES['file_r'] = [];
    }
    $former = new Former($_POST['name_user'],$_POST['email'],$_POST['message'],$_FILES['file_r']);

    echo json_encode($former->processing());

?>
